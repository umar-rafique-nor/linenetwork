﻿using UnityEngine;
using System.Collections;

public class Planer : MonoBehaviour {


	float positioX = 5000;
	float positioY = 5000;
	float scaleX;
	float scaleY;

	public GameObject plane;


	void Start(){
		scaleX = positioX/5;
		scaleY = positioY/5;
		//GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Plane);
		plane.transform.position = new Vector3 (positioX, 1, positioY);
		plane.transform.localScale = new Vector3 (scaleX, 3, scaleY);
		plane.transform.Rotate (Vector3.up * 180);

	
	}

}
