﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.IO;


public class VehiclManager : MonoBehaviour {


	HelperFunctions Hf = new HelperFunctions ();

	private float speedingFactor = 0.1f;


	float vXScale = 20;
	float vYScale = 20;
	float vZScale = 20;

	string timeStart = "1";

	bool readEvents = true;
	bool enter;
	//	C:\Users\umarr_000\Unity\XmlFile\InputData\Events.xml

	private string pathN = "C:\\Users\\umarr_000\\Unity\\XmlFile\\TimeTesterBilal5thSep\\Network.xml";
	private string pathE = "C:\\Users\\umarr_000\\Unity\\XmlFile\\TimeTesterBilal5thSep\\modified_events.xml";
	Dictionary<string, Vehicle> vehicles = new Dictionary<string, Vehicle> ();
	Dictionary<string, Movement> vehicleToMove = new Dictionary<string, Movement> ();

	void Start()
	{

		Hf.readNetwork (pathN, false);

		//vehicleStore (path);
		StartCoroutine(CallingUpdate());
	}


	IEnumerator CallingUpdate()
	{
		if (readEvents) 
		{
			
			XmlTextReader reader = null;
			reader = new XmlTextReader (pathE);
			reader.WhitespaceHandling = WhitespaceHandling.None;

			readEvents = false;
			var doc = new XmlDocument ();

			doc.Load (pathE);
			while (reader.Read ()) {
				if (reader.Name == "event") {
					
					string vId = reader.GetAttribute ("vehicle");
					string link = reader.GetAttribute ("link");
					string time = reader.GetAttribute ("time");
					string type = reader.GetAttribute ("type");
			
					if (type == "vehicle enters traffic" || type == "entered link")
					{
						
						float dt = float.Parse (reader.GetAttribute ("dt"));
						if (!vehicles.ContainsKey (vId)) 
						{
							
							GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);

							float xvalue = Hf.getLinks () [link].from.position.x;
							float yvalue = Hf.getLinks () [link].from.position.z;


							cube.transform.position = new Vector3 (xvalue, 0, yvalue);
							cube.transform.localScale = new Vector3 (vXScale, vYScale, vZScale);
							cube.GetComponent<Renderer> ().material.color = Color.green;


							Vehicle datavehicle = new Vehicle () {
								id = vId,
								vehicle = cube
							};
							vehicles.Add (datavehicle.id, datavehicle);
						}


							Movement movethisVehicle = new Movement () 
							{
								id = vId,
								vehicle = vehicles [vId].vehicle,
								to = Hf.getLinks () [link].to,
							speed = Hf.getLinks () [link].length/dt
							};
							vehicleToMove.Add (movethisVehicle.id, movethisVehicle);

					} 
					else if (type == "left link" || type == "vehicle leaves traffic") 
					{
						
						float distance = Vector3.Distance (vehicleToMove [vId].vehicle.transform.position, vehicleToMove [vId].to.position);
						while (distance > 0) 
						{
							foreach (string id in vehicleToMove.Keys) {

								vehicleToMove [id].vehicle.transform.position = Vector3.MoveTowards (vehicleToMove [id].vehicle.transform.position, vehicleToMove [id].to.position, vehicleToMove [id].speed * speedingFactor);
								if (vehicleToMove [id].speed < 27) {
									vehicleToMove [id].vehicle.GetComponent<Renderer> ().material.color = Color.red;
								} else {
									vehicleToMove [id].vehicle.GetComponent<Renderer> ().material.color = Color.green;
								}
								if (id == "1565" ) {
									print (vehicleToMove [id].speed);
								}
							}
							yield return new WaitForSeconds (0.0001f);
						    distance = Vector3.Distance (vehicleToMove [vId].vehicle.transform.position, vehicleToMove [vId].to.position);

						}
						vehicleToMove.Remove (vId);
					}
				

				}

			}
		}
	}

}
public class Vehicle
{

	public string id{ get; set;}
	public GameObject vehicle{ get; set;}

}

public class Movement
{

	public string id{ get; set;}
	public GameObject vehicle{ get; set;}
	public Transform to{ get; set;}
	public float speed{ get; set;}

}