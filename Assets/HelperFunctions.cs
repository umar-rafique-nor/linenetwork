﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.IO;

public class HelperFunctions : MonoBehaviour {


	private string path = "C:\\Users\\umarr_000\\Unity\\XmlFile\\TimeTesterBilal5thSep\\Network.xml";
	private bool display =  true;

	Dictionary<string, Node> nodes = new Dictionary<string, Node> ();
	Dictionary<string, Link> links = new Dictionary<string, Link> ();


	void Start()
	{
		display =  true;
		readNetwork (path, display);

	}


	public Dictionary<string, Node> getNodes(){

		return nodes;
	}

	public Dictionary<string, Link> getLinks(){

		return links;
	}


	public void readNetwork(string path, bool display)
	{
		float xScale = 10;
		float yScale = 10;
		float zScale = 10;

		XmlTextReader reader = null;
		reader = new XmlTextReader (path);
		reader.WhitespaceHandling = WhitespaceHandling.None;


		var docu = new XmlDocument ();

		docu.Load (path);
		while (reader.Read ()) {
			if (reader.Name == "node") {
//			double latitude = Double.Parse (node.Attributes ["y"].Value);
//			double longitude = Double.Parse (node.Attributes ["x"].Value);
//
//			string id = node.Attributes ["id"].Value;

				double latitude = Double.Parse (reader.GetAttribute ("y"));
				double longitude = Double.Parse (reader.GetAttribute ("x"));
				string id = reader.GetAttribute ("id");


				MappedData md = Mapper (latitude, longitude);

				float x = (float)md.Xspot;
				float y = (float)md.Yspot;

				GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
				cube.transform.position = new Vector3 (x, 0, y);
				cube.transform.localScale = new Vector3 (xScale, yScale, zScale);
				cube.GetComponent<Renderer> ().material.color = Color.black;

				if (display == true) {
					cube.SetActive (true);

				} else {
					cube.SetActive (false);
				}
				Node dataNode = new Node () {
					id = id,
					node = cube
				};

				nodes.Add (dataNode.id, dataNode);

			}

		}
		// Getting Links

		XmlTextReader reader1 = null;
		reader1 = new XmlTextReader (path);
		reader1.WhitespaceHandling = WhitespaceHandling.None;
		var docum = new XmlDocument ();
		docum.Load (path);
		while (reader1.Read ()) {
			if (reader1.Name == "link") {



//				string UniqId = link.Attributes ["id"].Value;
//
//				Node from_node = nodes [link.Attributes ["from"].Value];
//				Node to_node = nodes [link.Attributes ["to"].Value];
					
				Node from_node = nodes [reader1.GetAttribute ("from")];
				Node to_node = nodes [reader1.GetAttribute ("to")];
				string UniqId = reader1.GetAttribute ("id");
				float length = float.Parse (reader1.GetAttribute ("length"));


				Link datalink = new Link () {
					id = UniqId,
					from = from_node.node.transform,
					to = to_node.node.transform,
					length = length
				};

				links.Add (datalink.id, datalink);

			}
		}
		} 
	MappedData Mapper(double latitude, double longitude)
	{

		// doing the transformation

		double long1 = 584561.33;
		double x1 = 8096.7;
		double lat1 = 4206843.85;
		double y1 = 7656.6;


		double long2 = 550102.04;
		double x2 = 5301.1;
		double lat2 = 4146931.57;
		double y2 = 2831;


		double x;
		double y;


		double m_long  = (x2 - x1)/(long2-long1);
		double m_lat   = (y2 - y1)/(lat2-lat1);


		x = m_long * (longitude - long1) + x1;
		y = m_lat * (latitude - lat1) + y1;

		//        print ("Transformed point");
		//        print (x + "," +y);

		//float xpoint = (float) x;
		//float ypoint = (float) y;



		MappedData data1 = new MappedData()
		{
			Xspot = x,
			Yspot = y
		};


		return data1;

	}




		
}



public class Node
{

	public string id{ get; set;}
	public GameObject node{ get; set;}

}

public class Link
{

	public string id{ get; set;}
	public Transform from{ get; set;}
	public Transform to{ get; set;}
	public float length{ get; set;}
}

public class MappedData
{

	public double Xspot{ get; set;}
	public double Yspot{ get; set;}

}
































// how to get the position of one node 
/*
for (int k =1; k <= 15; k++)
{

	Transform tr = nodes [k.ToString ()].node.transform;
	print (tr.position);

}*/