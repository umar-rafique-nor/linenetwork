﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using System.IO;

public class VehicleStore : MonoBehaviour {


	HelperFunctions Hf = new HelperFunctions ();

	float vXScale = 50;
	float vYScale = 50;
	float vZScale = 50;

	string timeStart = "1";
	float speed = 1000f;
	bool readEvents = true;
	bool enter;
//	C:\Users\umarr_000\Unity\XmlFile\InputData\Events.xml

	private string pathN = "C:\\Users\\umarr_000\\Unity\\XmlFile\\InputData\\Network.xml";
	private string pathE = "C:\\Users\\umarr_000\\Unity\\XmlFile\\InputData\\Events.xml";
	Dictionary<string, Vehicle1> vehicles = new Dictionary<string, Vehicle1> ();
	Dictionary<string, Move> vehicleToMove = new Dictionary<string, Move> ();



	void Start()
	{
		
		Hf.readNetwork (pathN, false);

		//vehicleStore (path);
		StartCoroutine(CallingUpdate());
	}




	IEnumerator CallingUpdate()
	{
		if (readEvents) {
			XmlTextReader reader = null;
			reader = new XmlTextReader(pathE);
			reader.WhitespaceHandling = WhitespaceHandling.None;
			readEvents = false;
			var doc = new XmlDocument ();

			doc.Load (pathE);
////			foreach (XmlNode node in doc.SelectNodes("//events//event[@vehicle]")) {
//				
////				string vId = node.Attributes ["vehicle"].Value;
////				//string action = node.Attributes ["action"].Value;
////				string link = node.Attributes ["link"].Value;
////				string time = node.Attributes ["time"].Value;
			while (reader.Read()) {
				if (reader.Name == "event") 
				{

					string vId = reader.GetAttribute ("vehicle");
					string link = reader.GetAttribute ("link");
					string time = reader.GetAttribute ("time");
					string type = reader.GetAttribute ("type");
					float getSpeed = float.Parse(reader.GetAttribute ("speed"));


////					print (vId + " >> " + link + "  " + time);

				if (!vehicles.ContainsKey (vId)) {
					GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
			
					float xvalue = Hf.getLinks () [link].from.position.x;
					float yvalue = Hf.getLinks () [link].from.position.z;


					cube.transform.position = new Vector3 (xvalue, 0, yvalue);
					cube.transform.localScale = new Vector3 (vXScale, vYScale, vZScale);
					cube.GetComponent<Renderer>().material.color = Color.red;
			

					Vehicle datavehicle = new Vehicle () {
						id = vId,
						vehicle = cube
					};


					vehicles.Add (datavehicle.id, datavehicle);
				}


				if (time == timeStart) {
						Move movethisVehicle = new Move () {
						id = vId,
						vehicle = vehicles [vId].vehicle,
						to = Hf.getLinks () [link].to,
							speed = getSpeed
					};
					vehicleToMove.Add (movethisVehicle.id, movethisVehicle);
				}


				else
				{
					timeStart = time;
					foreach (string id in vehicleToMove.Keys) 
					{
						print ("time is " +time);
////						print ("id " + id + " = " +vehicleToMove [id].vehicle.transform.position);
////						print ("to  = " +vehicleToMove [id].to.position);
////						print("--------------");

					}
					float distence = 0;
					enter = true;


					while (distence > 0 || enter) {
////					for(int i=0; i<10; i++){
						
						distence = 0;
						enter = false;
						foreach (string id in vehicleToMove.Keys) {
							
								vehicleToMove [id].vehicle.transform.position = Vector3.MoveTowards (vehicleToMove [id].vehicle.transform.position, vehicleToMove [id].to.position, vehicleToMove [id].speed);
							distence = distence + Vector3.Distance (vehicleToMove [id].vehicle.transform.position, vehicleToMove [id].to.position);

						}
					//	print (distence);
						yield return new WaitForSeconds (0.0001f);
							Destroy (vehicleToMove[vid]);

					}
					vehicleToMove.Clear ();

						Move movethisVehicle = new Move () {
						id = vId,
						vehicle = vehicles [vId].vehicle,
						to = Hf.getLinks () [link].to
					};
					vehicleToMove.Add (movethisVehicle.id, movethisVehicle);
				}

			}
		}

}

	}

	IEnumerator wait()
	{
		yield return new WaitForSeconds (0.01f);
	}




	public Dictionary<string, Vehicle> getVehicles(){

		return vehicles;
	}


	public void vehicleStore(string path)
	{


		var doc = new XmlDocument ();

		doc.Load (path);
		foreach (XmlNode node in doc.SelectNodes("//events//event[@vehicle]"))
		{
			string vId = node.Attributes ["vehicle"].Value;
			string action = node.Attributes ["action"].Value;
			string link = node.Attributes ["link"].Value;


			if (!vehicles.ContainsKey (vId)) {

				//print (vId + "Vehicle is created");
				GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);

				float xvalue = Hf.getLinks () [link].from.position.x;
				float yvalue = Hf.getLinks () [link].from.position.z;
				cube.transform.position = new Vector3 (xvalue, 0, yvalue);
				cube.transform.localScale = new Vector3 (vXScale, vYScale, vZScale);
				cube.GetComponent<Renderer>().material.color = Color.green;


				Vehicle datavehicle = new Vehicle () {
					id = vId,
					vehicle = cube
				};
					

				vehicles.Add (datavehicle.id, datavehicle);

				Move movethisVehicle = new Move () {
					id = vId,
					vehicle = cube,
					to = Hf.getLinks () [link].to,
				};
				vehicleToMove.Add (movethisVehicle.id, movethisVehicle);

			} else {
			//	print (vId + "Vehicle Already exist in Dictionary");

				//print (vehicles [vId].vehicle);
			}
		}

	//TODO: iterate over vehiclesToMove dict

		foreach (string id in vehicleToMove.Keys) 
		{
//			print ("id " + id + " = " +vehicleToMove [id].vehicle.transform.position);
//			print ("to  = " +vehicleToMove [id].to.position);
			print("--------------");
		}

		vehicleToMove.Clear ();

		print("Printing after clearing");


		foreach (string id in vehicleToMove.Keys) 
		{
//			print ("id " + id + " = " +vehicleToMove [id].vehicle.transform.position);
//			print ("to  = " +vehicleToMove [id].to.position);
//			print("--------------");
		}

	// Print all the vehicles position.

	//TODO: Clear vehiclesToMove dict

	//TODO: iterate over vehiclesToMove dict
	// Print all the vehicles position.


	}
}



public class Vehicle1
{

	public string id{ get; set;}
	public GameObject vehicle{ get; set;}

}

public class Move
{

	public string id{ get; set;}
	public GameObject vehicle{ get; set;}
	public Transform to{ get; set;}
	public float speed{ get; set;}

}